const canvas = document.getElementById("canvas");
const ctx = canvas.getContext("2d");

/*------- load images (background and apple) ---*/
let bgImg = new Image();
bgImg.src = "bg.png";

let food = new Image();
food.src = "food.png";

/*---------------- load audios -----------------*/
let eat = new Audio();
let dead = new Audio();
let up = new Audio();
let down = new Audio();
let left = new Audio();
let right = new Audio();

eat.src = "audio/eat.mp3";
dead.src = "audio/dead.mp3";
up.src = "audio/up.mp3";
down.src = "audio/down.mp3";
left.src = "audio/left.mp3";
right.src = "audio/right.mp3";
/*----------- create variables ----------------*/
let unit = 32;

let snake = [];
snake[0] = {
  x: 9*unit,
  y: 10*unit
}


let apple = {
  x: Math.floor(Math.random()*17+1)*unit,
  y: Math.floor(Math.random()*15+3)*unit
}

let score = 0;


/*------------------- create functions ------------*/
function draw(){
  // draw bg image
  ctx.drawImage(bgImg,0,0);

  // draw snake image
  for (var i = 0; i < snake.length; i++) {
    // draw one rect each time
    ctx.fillStyle = (i == 0) ? "green" : "white";
    ctx.fillRect(snake[i].x,snake[i].y,unit,unit);

    // draw contour of rect
    ctx.strokeStyle = "red";
    ctx.strokeRect(snake[i].x,snake[i].y,unit,unit);
  }

  // draw apple image
  ctx.drawImage(food,apple.x,apple.y);

  // old head position
  let snakeX = snake[0].x;
  let snakeY = snake[0].y;

  // new head possible movements
  if(d == "RIGHT"){ snakeX += unit; }
  if(d == "LEFT"){ snakeX -= unit; }
  if(d == "DOWN"){ snakeY += unit; }
  if(d == "UP"){ snakeY -= unit; }

  // if snake reaches the apple
  if (snakeX == apple.x && snakeY == apple.y) {
    eat.play();
    score++;
    // create new apple
    apple = {
      x: Math.floor(Math.random()*17+1)*unit,
      y: Math.floor(Math.random()*15+3)*unit
    }
  }else {
    // remove tail
    snake.pop();
  }
  // add new head
  let newHead = {
    x: snakeX,
    y: snakeY
  };

  // set game over rules
  if (snakeX < unit || snakeX > 17*unit || snakeY < 3*unit || snakeY > 17*unit || collision(newHead,snake) == true) {
    dead.play();
    clearInterval(game);
  }

  // add new head
  snake.unshift(newHead);

  // set score
  ctx.font = "45px changa one";
  ctx.fillStyle = "white";
  ctx.fillText(score,4*unit,1.6*unit);

}
let game = setInterval(draw,100);

/*------- control snake --------*/
let d;
document.addEventListener("keydown",function(event){
  if (event.keyCode == 37 && d != "RIGHT") {
    left.play();
    d = "LEFT";
  }
  if (event.keyCode == 38 && d != "DOWN") {
    up.play();
    d = "UP";
  }
  if (event.keyCode == 39 && d != "LEFT") {
    right.play();
    d = "RIGHT";
  }
  if (event.keyCode == 40 && d != "UP") {
    down.play();
    d = "DOWN";
  }
});


/*------------- collision function -------------*/
function collision(head,snake){
  for (var i = 0; i < snake.length; i++) {
    if (head.x == snake[i].x && head.y == snake[i].y) {
      return true;
    }
    return false;
  }
}
